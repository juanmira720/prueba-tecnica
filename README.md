# Archivo de instruccion para configurar projecto

## Requisitos
- Java 17
- Node v 18.15.0
- PostgreSQL

## Post Requisitos 
- Crear una base de datos llamada: tecnictest, tal como lo indica el application.properties

# Frontend
- Para ingresar al frontend angular
```
    cd frontend 
```
- Para instalar dependencias
```
    npm install
```
- Para correr el servidor
```
    ng serve    
```

# Backend
- Para ingresar a la carpeta backend
```
    cd backend
```
- Instalar dependencias con maven
```
    ./mvnw install
```
- Ya una vez instalado
```
    ./mvnw spring-boot:run
```

