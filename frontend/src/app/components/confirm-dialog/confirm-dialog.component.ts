import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

interface DialogData {
  id: number;
  firstName: string;
  lastName: string;
}

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css'],
})
export class ConfirmDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _userService: UserService,
    private _snackbar: MatSnackBar
  ) {}

  closeDialog() {
    console.log('Closed');
  }

  performDelete(id: number) {
    this._userService.deleteUser(id).subscribe((value) => {
      if (value)
        this._snackbar.open('eliminado correctamente', '', {
          horizontalPosition: 'right',
          verticalPosition: 'bottom',
          duration: 2 * 1000,
        });
      else this._snackbar.open('Error al eliminar', '', {
        horizontalPosition: 'right',
        verticalPosition: 'bottom',
        duration: 2 * 1000,
      });;
    });
  }
}
