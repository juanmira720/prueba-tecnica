import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../types/User';
import { serverUrl } from '../constants';
import { ResponseUserCreatedDto, ResponseUserDto } from '../types/Response.dto';
import { RequestUserDto } from '../types/Request.dto';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _users = new BehaviorSubject<User[]>([]);
  public users = this._users.asObservable();

  constructor(private _http: HttpClient) {}

  getAllUsers(page: number = 0): Observable<ResponseUserDto> {
    return this._http.get<ResponseUserDto>(
      serverUrl + `/user/users?page=${page}`
    );
  }

  deleteUser(id: number) {
    return this._http.delete<boolean>(serverUrl + `/user/${id}`);
  }

  createUser(user: RequestUserDto) {
    return this._http.post<ResponseUserCreatedDto>(
      serverUrl + `/user/save`,
      user
    );
  }
}
