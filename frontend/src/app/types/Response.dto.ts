import { User } from './User';

export type ResponseUserDto = {
  data: Data;
  message: string;
  status: string;
  statusCode: number;
  timeStamp: Date;
};

export type ResponseUserCreatedDto = {
  errors: Errors;
  data: User;
  message: string;
  status: string;
  statusCode: number;
  timeStamp: Date;
};

type Data = {
  users: Content;
};

type Errors = {
  firstName: string;
  lastName: string;
  otherName: string;
  country: string;
};

type Content = {
  content: User[] | User;
  totalPages: number;
  totalElements: number;
};
