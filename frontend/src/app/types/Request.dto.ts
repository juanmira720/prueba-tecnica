export type RequestUserDto = {
  firstName: string;
  lastName: string;
  country: string;
  otherName: string;
};
