export type CreateUserValidation = {
  errors: {
    firstName: string;
    lastName: string;
    otherName: string;  
  };
};
