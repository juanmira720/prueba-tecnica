export type User = {
    id:number;
    firstName:string;
    lastName:string;
    otherName:string;
    email:string;
    country:string;
}
