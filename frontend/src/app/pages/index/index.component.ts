import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/types/User';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
})
export class IndexComponent implements OnInit {
  constructor(private _userService: UserService, private _dialog: MatDialog) {}
  users: User[] = [];
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngOnInit(): void {
    this.getAllusers();
  }

  onPageChange(event: PageEvent) {
    const pageIndex = event.pageIndex;
    this.getAllusers(pageIndex);
  }

  getAllusers(page: number = 0) {
    this._userService.getAllUsers(page).subscribe((value) => {
      if (Array.isArray(value.data.users.content))
        this.users = value.data.users.content;
      this.dataSource = new MatTableDataSource(this.users);
      this.paginator.length = value.data.users.totalElements;
    });
  }

  deleteUser(id: number) {
    this._dialog
      .open(ConfirmDialogComponent, {
        width: '350px',
        enterAnimationDuration: 1,
        exitAnimationDuration: 1,
        hasBackdrop: true,
        data: {
          id,
          firstName: this.users.find((x) => x.id == id)?.firstName,
          lastName: this.users.find((x) => x.id == id)?.lastName,
        },
      })
      .afterClosed()
      .subscribe((value) => {
        this.getAllusers();
        this.paginator.pageIndex = 0;
      });
  }

  displayedColumns: string[] = [
    'id',
    'firstName',
    'lastName',
    'otherName',
    'email',
    'country',
    'actions',
  ];
}
