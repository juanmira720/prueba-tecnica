import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { RequestUserDto } from 'src/app/types/Request.dto';
import { CreateUserValidation } from 'src/app/types/Validation';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent {
  constructor(
    private _userService: UserService,
    private _snackBar: MatSnackBar,
    private _router: Router
  ) {}

  userForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    otherName: new FormControl(''),
    country: new FormControl('CO'),
  });

  handlers: CreateUserValidation = {
    errors: {
      firstName: '',
      lastName: '',
      otherName: '',
    },
  };

  handleSuccessCreation() {
    this.openSnackBar();
    this._router.navigate(['']);
  }

  handleErrorCreation(value: any) {
    console.log(value);    
    this.handlers.errors.firstName = value.error.errors.firstName;
    this.handlers.errors.lastName = value.error.errors.lastName;
    this.handlers.errors.otherName = value.error.errors.otherName;

    console.log(this.handlers);
  }

  openSnackBar() {
    this._snackBar.open('Usuario creado correctamente', '', {
      duration: 2 * 1000,
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
    });
  }

  onSubmit($event: any) {
    $event.preventDefault();
    this._userService
      .createUser(this.userForm.value as RequestUserDto)
      .subscribe({
        next: this.handleSuccessCreation.bind(this),
        error: this.handleErrorCreation.bind(this),
      });
  }
}
