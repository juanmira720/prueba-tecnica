package com.prueba.tecnic.Dto;


import com.prueba.tecnic.entities.Country;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {    

    @NotEmpty()    
    @Pattern(regexp = "^[a-zA-Z\\s]*", message = "FirstName no debe poseer caracteres especiales")
    @Size(max = 20)
    private String firstName;

    @NotEmpty()
    @Pattern(regexp = "^[a-zA-Z\\s]*", message = "LastName no debe poseer caracteres especiales")
    @Size(max = 20)
    private String lastName;

    @Size(max = 50)
    @Pattern(regexp = "^[a-zA-Z\\s]*", message = "OtherName no debe poseer caracteres especiales")
    private String otherName;

    @NotNull(message = "Country es obligatorio")
    private Country country;
}
