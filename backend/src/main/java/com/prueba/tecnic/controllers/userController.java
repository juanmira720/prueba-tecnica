package com.prueba.tecnic.controllers;

import java.time.LocalDateTime;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.tecnic.Dto.UserDto;
import com.prueba.tecnic.entities.Response;
import com.prueba.tecnic.services.UserServiceImplementation;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class userController {
    private final UserServiceImplementation userService;

    @GetMapping("/users")
    public ResponseEntity<Response> getUsers(@RequestParam(defaultValue = "0") int page) {
        return ResponseEntity.ok(Response.builder().timeStamp(LocalDateTime.now())
                .data(Map.of("users", userService.getAllUsers(10, page)))
                .message("List users retrieved")
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .build());
    }

    @PostMapping("/save")
    public ResponseEntity<Response> saveUser(@Valid @RequestBody UserDto userDto) {
        return ResponseEntity.ok(Response.builder().timeStamp(LocalDateTime.now())
                .data(Map.of("user", userService.createUser(userDto)))
                .message("User created")
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> deleteUser(@PathVariable("id") Long id) {
        return ResponseEntity.ok(Response.builder().timeStamp(LocalDateTime.now())
                .data(Map.of("user", userService.deleteUser(id)))
                .message("User deleted")
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .build());
    }
}
