package com.prueba.tecnic.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prueba.tecnic.entities.Usuario;

public interface UserRepository extends JpaRepository<Usuario, Long>{
    Optional<Usuario> findByEmail(String email);
}
