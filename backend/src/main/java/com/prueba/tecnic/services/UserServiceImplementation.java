package com.prueba.tecnic.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.prueba.tecnic.Dto.UserDto;
import com.prueba.tecnic.entities.Country;
import com.prueba.tecnic.entities.Usuario;
import com.prueba.tecnic.repositories.UserRepository;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
@Transactional
public class UserServiceImplementation implements IUSerService {

    private final UserRepository userRepository;

    @Override
    public String createEmail(String firstName, String lastName, Country country) {
        String domain = country == Country.CO ? "@jvntecnolgias.com" : "@jvntecnologias.com.us";

        String premail = firstName.toLowerCase() + "." + lastName.toLowerCase();
        String email = premail + domain;

        int count = 1;

        while (userRepository.findByEmail(email).isPresent()) {
            email = premail + "." + count + domain;
            count++;
        }

        return email;
    }

    @Override
    public Page<Usuario> getAllUsers(int limit, int page) {
        return userRepository.findAll(PageRequest.of(page, limit));
    }

    @Override
    public Usuario createUser(UserDto user) {
        Usuario newUser = new Usuario();
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setOtherName(user.getOtherName() != null ? user.getOtherName() : "");
        newUser.setCountry(user.getCountry());
        newUser.setEmail(this.createEmail(user.getFirstName(), user.getLastName(), user.getCountry()));
        return userRepository.save(newUser);
    }

    @Override
    public Boolean deleteUser(Long id) {
        try {
            userRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
