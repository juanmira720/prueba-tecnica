package com.prueba.tecnic.services;

import org.springframework.data.domain.Page;

import com.prueba.tecnic.Dto.UserDto;
import com.prueba.tecnic.entities.Country;
import com.prueba.tecnic.entities.Usuario;

public interface IUSerService {
    String createEmail(String firstName, String lastName, Country country);

    Page<Usuario> getAllUsers(int limit, int page);

    Usuario createUser(UserDto user);    

    Boolean deleteUser(Long id);
}
