package com.prueba.tecnic.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
@AllArgsConstructor
public class Usuario {
    public Usuario() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    // @Size(max = 20)
    private String firstName;

    @Column(nullable = false)
    // @Size(max = 20)
    private String lastName;

    @Column(nullable = true)
    // @Size(max = 50)
    private String otherName;

    @Column(nullable = false, unique = true)
    private String email;

    @Column
    private Country country;
}
