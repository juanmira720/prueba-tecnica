package com.prueba.tecnic.entities;

public enum Country {
    CO("CO"),
    USA("USA");

    private final String country;

    Country(String country) {
        this.country = country;
    }

    public String getCountry() {
        return this.country;
    }
}
