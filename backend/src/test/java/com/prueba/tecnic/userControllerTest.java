package com.prueba.tecnic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.prueba.tecnic.Dto.UserDto;
import com.prueba.tecnic.controllers.userController;
import com.prueba.tecnic.entities.Country;
import com.prueba.tecnic.entities.Usuario;
import com.prueba.tecnic.services.UserServiceImplementation;

@RunWith(SpringRunner.class)
@WebMvcTest(userController.class)
public class userControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImplementation userService;

    Logger logger = LoggerFactory.getLogger(userServiceTest.class);

    @Test
    public void testCreateUser_Success() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setFirstName("Juan");
        userDto.setLastName("Mira");
        userDto.setCountry(Country.CO);

        Mockito.when(userService.createUser(Mockito.any(UserDto.class))).thenReturn(new Usuario());
        String json = "{\"firstName\":\"Juan\",\"lastName\":\"Mira\", \"country\":\"CO\"}";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/user/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        logger.info(result.getResponse().getContentAsString());
    }

    @Test
    public void testCreateUser_Failure() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setFirstName("Juan");
        userDto.setLastName("Mira");
        userDto.setCountry(Country.CO);

        Mockito.when(userService.createUser(Mockito.any(UserDto.class))).thenReturn(new Usuario());
        String json = "{\"firstName\":\"Juanñ\",\"lastName\":\"Mira\", \"country\":\"CO\"}";
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/user/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(MockMvcResultMatchers.status().isBadRequest()).andReturn();
        logger.info(result.getResponse().getContentAsString());
    }
}
