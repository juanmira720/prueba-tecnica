package com.prueba.tecnic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.prueba.tecnic.Dto.UserDto;
import com.prueba.tecnic.entities.Country;
import com.prueba.tecnic.entities.Usuario;
import com.prueba.tecnic.services.UserServiceImplementation;

@RunWith(SpringRunner.class)
@SpringBootTest
public class userServiceTest {

        @Autowired
        private UserServiceImplementation userService;

        Logger logger = LoggerFactory.getLogger(userServiceTest.class);

        // prueba para verificar el servicio de crear usuario
        @Test
        public void testCreateUser() throws Exception {
                UserDto userDto = new UserDto();
                userDto.setFirstName("Juan");
                userDto.setLastName("LastName");
                userDto.setCountry(Country.CO);

                Usuario user = userService.createUser(userDto);

                assertTrue(user != null);
        }

        // verificar validaciones del dto
        @Test
        public void testCreateEmail() throws Exception {
                UserDto userDto1 = new UserDto();
                userDto1.setFirstName("Juan");
                userDto1.setLastName("Mira");
                userDto1.setCountry(Country.CO);

                UserDto userDto2 = new UserDto();
                userDto2.setFirstName("Juan");
                userDto2.setLastName("Mira");
                userDto2.setCountry(Country.CO);

                Usuario user1 = userService.createUser(userDto1);
                Usuario user2 = userService.createUser(userDto2);

                assertEquals(user1.getEmail(), "juan.mira@jvntecnolgias.com");
                assertEquals(user2.getEmail(), "juan.mira.1@jvntecnolgias.com");

                userService.deleteUser(user1.getId());
                userService.deleteUser(user2.getId());
        }
}
